// @ts-check
'use strict';

const fs = require('fs');
const conf = require('../conf.js');

const prepare = () => {
  if (fs.existsSync(conf.CURRENT)) {
    fs.renameSync(conf.CURRENT, conf.PREV);
  }
};

module.exports = prepare;
