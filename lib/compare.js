// @ts-check

const looksSame = require('looks-same');
const colors = require('colors/safe.js');
const conf = require('../conf.js');

const defaultFunc = (err, equal) => {
  if (equal) {
    process.exit(0);
  } else if (err) {
    console.log(colors.red('ERROR: in comparing images'));
    console.log(colors.red(err));
    process.exit(1);
  } else {
    // @ts-ignore
    console.log(colors.yellow.bold('====== DIFFERENCE DETECTED ======'));
    console.log('Generating diff image....');
    looksSame.createDiff(conf.DIFF_CONF, err => {
      if (err) {
        console.log(colors.red(err));
      }
    });
  }
};

const compare = (current, prev, func) => {
  looksSame(current, prev, func);
};

module.exports = {
  compare,
  defaultFunc
};
