// @ts-check
'use strict';

const puppeteer = require('puppeteer');
const conf = require('../conf.js');

const capture = async () => {
  // https://github.com/Googlechrome/puppeteer/issues/290
  // --no-sandboxをつけないとエラーになった
  const browser = await puppeteer.launch({ args: ['--no-sandbox'] });
  const page = await browser.newPage();
  await page.goto(conf.URL, { waitUntil: 'domcontentloaded' });
  await page.screenshot({ fullPage: true, path: conf.CURRENT });
  await browser.close();
};

module.exports = capture;
