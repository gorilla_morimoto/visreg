// @ts-check
'use strict';

const fs = require('fs');

const prepare = require('./lib/prepare.js');
const capture = require('./lib/capture.js');
const compare = require('./lib/compare.js');

const conf = require('./conf.js');

(async () => {
  prepare();
  await capture();
  if (!fs.existsSync(conf.PREV)) {
    process.exit(0);
  }
  compare.compare(conf.CURRENT, conf.PREV, compare.defaultFunc);
})();
