# visreg

2018-06-16

[`puppeteer`](https://github.com/GoogleChrome/puppeteer)と[`looks-same`](https://github.com/gemini-testing/looks-same)を使ってwebコーディングの回帰テストを行う

## 挙動

1. カレントディレクトリに`<CURRENT>`という名前のファイルが存在したら、`<PREV>`にリネームする

2. `<URL>`にアクセスしてフルページのスクリーンショット撮影、`<CURRENT>`という名前でカレントディレクトリに保存する

3. `<CURRENT>`と`<PREV>`を比較し、差分がなければ終了（ステータス0）、差分があれば`diff.png`という名前で差分を出力して終了（ステータス1）


## 使い方

### `conf.js`に設定を記述する

- `URL`: テスト対象のURL
- `PREV`   : 変更前のスクリーンショットのファイル名
- `CURRENT`: 現在のページのスクリーンショットのファイル名

### 実行

以下のコマンドでカレントディレクトリに現在の状態のスクリーンショットが`<CURRENT>`という名前で保存される

```sh
$ node index.js
```

もし差分が発生していた場合、`diff.png`という名前で差分イメージが保存される


### テスト

```sh
$ npx jest
```
